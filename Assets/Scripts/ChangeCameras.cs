﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCameras : MonoBehaviour
{
    public GameObject mainCameraChange;
    public GameObject subCameraChange;

    // Start is called before the first frame update
    void Start()
    {
        subCameraChange.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnValueChanged()
    {
        subCameraChange.SetActive(true);
        mainCameraChange.SetActive(false);
        if(GetComponent<Toggle>().isOn == false)
        {
            subCameraChange.SetActive(false);
        }
    }
}
