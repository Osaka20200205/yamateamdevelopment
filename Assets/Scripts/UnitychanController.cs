﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitychanController : MonoBehaviour
{
    // Unityちゃんアタッチ
    public GameObject unityChan;

    // Unityちゃんの移動スピード
    public float moveSpeed = 5;

    // Unityちゃん移動
    Vector3 moveDirection = Vector3.zero;

    Rigidbody unityChanRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        unityChanRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        // Unityちゃん移動処理
        moveDirection.x = Input.GetAxisRaw("Horizontal");
        moveDirection.z = Input.GetAxisRaw("Vertical");
        moveDirection.Normalize();
        unityChanRigidbody.velocity = moveDirection * moveSpeed;
    }
}
